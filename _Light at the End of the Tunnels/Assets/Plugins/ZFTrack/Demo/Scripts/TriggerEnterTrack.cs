﻿namespace ZenFulcrum.Track
{

    using UnityEngine;
    using System.Collections;

    public class TriggerEnterTrack : MonoBehaviour
    {
        public TrackSwitcher m_Switcher;

        public void OnTriggerEnter()
        {
            Debug.Log("Tracks Switched!");
            m_Switcher.Switch();
        }
    }

}