/**
 * <copyright>
 * Tracks and Rails Asset Package by Zen Fulcrum
 * Copyright 2015 Zen Fulcrum LLC
 * Usage is subject to Unity's Asset Store EULA (https://unity3d.com/legal/as_terms)
 * </copyright>
 */

namespace ZenFulcrum.Track {

using UnityEngine;
using System.Collections;

public class TouchSwitch : MonoBehaviour {
	public TrackSwitcher switcher;

        void Update()
        {
            //Detect when the Return key is pressed down
            if (Input.GetKeyDown(KeyCode.A))
            {
                switcher.Switch();
                Debug.Log("Return key was pressed.");
            }

            //Detect when the Return key has been released
           // if (Input.GetKeyUp(KeyCode.Return))
           // {
           //     Debug.Log("Return key was released.");
           // }
        }

        public void OnMouseDown() {
		switcher.Switch();	
	}
}

}
