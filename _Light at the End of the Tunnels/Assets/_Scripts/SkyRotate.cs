﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyRotate : MonoBehaviour {

    public float SkyboxSpeed = 1f;
    public Material skyMaterial;

    void Update()
    {
        skyMaterial.SetFloat("_Rotation", Time.time * SkyboxSpeed);
    }
}